# rboxr
## Description
Audio player written in Racket

### Features

- [ ] Play entire default playlist
- [ ] Search through entire default playlist
- [x] Save library metadata in RKTD
- [x] Preferences in standard location, but moveable RKTD
- [ ] Playlists (PLS or M3U?)
  * [x] Add/remove tracks
  * [ ] Export playlist to disk (in M3U format)
  * [ ] Import playlist
  * [ ] Reorder playlist tracks (only for order-added, separate from sort by metadata columns)
- [ ] Normal audio controls
  * [ ] Play/Pause
  * [ ] Previous
  * [ ] Next
  * [ ] Repeat/Repeat 1 (toggle type on-click: turn on repeat, if repeat then repeat1, if repeat1 both off)
  * [ ] Shuffle
  * [ ] Volume
  * [ ] Mute
- [ ] Responds to media controls
- [ ] Rescan metadata of current selection or of multiple highlighted tracks
- [ ] Edit metadata of tracks
- [ ] Monitor Music directory for changes and auto-import
- [ ] Optionally remember last song played on program start


### Data Structures

The data structures are written with [txexprs](https://docs.racket-lang.org/txexpr/index.html)
or "tagged x-expressions". They are expressive, arbitrary in structure, and can
be saved to disk as-is then imported on program launch without any sort of
conversion.

Track example:
```racket
(track
 ((date-added "2022-07-26")
  (date-last-played "2022-07-26")
  (play-count "5")
  (lyrics "false")
  (album-art "false"))
 (metadata
  (title "rsound-example")
  (artist "jbclements")
  (album "")
  (composer "")
  (genre "Game")
  (track-number "01")
  (track-total "")
  (disc-number "")
  (disc-number-total "")
  (year "")
  (bpm "")
  (comment "Example track from rsound documentation")
  (duration "00:08"))
 (file-path "~/prog/rboxr/playground/rsound-example.wav"))
```

Playlist example:
```racket
(playlist
 ((name "playlist-name") (track-count "n"))
 (track ...)
 (track ...)
 ...)
```

"Library" refers to the entirety of tracked tracks and playlists. The playlist
with every track in the library is given the name "default".

Library example:
```racket
(library
 (playlist
   ((name "default") (track-count "n"))
   (track ...)
   (track ...)
   ...)
 (playlist
  ((name "playlist-name") (track-count "n"))
  (track ...)
  ...))
```

### Dependencies

The following libraries can be installed with corresponding command-line calls
to `raco pkg install ...`

- [binary-class-mp3](https://github.com/Kalimehtar/binary-class-mp3)
- [binary-class-riff](https://github.com/lwhjp/binary-class-riff)
- [rsound](https://github.com/jbclements/RSound)
- [txexpr](https://git.matthewbutterick.com/mbutterick/txexpr)

## License
rboxr - an audio player written in Racket
Copyright (C) 2022 Lehi Toskin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
