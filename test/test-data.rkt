#lang racket/base
; test-data.rkt
(require rackunit txexpr "../data.rkt")

(define-binary-check (check-string=? string=? actual expected))

(define library (make-library))
(define playlist (txexpr 'playlist '((name "foo") (track-count "0"))))
(define track
  '(track
    ((date-added "2022-07-26")
     (date-last-played "2022-07-26")
     (play-count "5")
     (lyrics "false")
     (album-art "false"))
    (metadata
     (title "rsound-example")
     (artist "jbclements")
     (album "")
     (composer "")
     (genre "Game")
     (track-number "01")
     (track-total "")
     (disc-number "")
     (disc-number-total "")
     (year "")
     (bpm "")
     (comment "Example track from rsound documentation")
     (duration "00:08"))
    (file-path "~/prog/rboxr/playground/rsound-example.wav")))

(test-case "booleans"
  (check-true (library? library))
  (check-true (library-empty? library))
  (check-true (playlist? playlist))
  (check-true (playlist-empty? playlist)))

; these three should have the same behavior
(test-case "Adding playlist to library"
  (check-equal? (make-library playlist) (set-playlist library playlist))
  (check-equal? (make-library playlist) (add-playlist library playlist))
  (check-equal? (set-playlist library playlist) (add-playlist library playlist))
  (check-false (library-empty? (make-library playlist))))

(test-case "Playlist booleans"
  (check-equal? (make-playlist "foo") playlist)
  (check-true (playlist-empty? playlist))
  (check-true (playlist-name=? playlist "foo"))
  (check-true (playlist-name=? (get-playlist library "default") "default")))

(test-case "Removing playlist"
  (define l1 (add-playlist library playlist))
  (check-equal? (get-playlist l1 "foo") playlist)
  (define l0 (del-playlist l1 "foo"))
  (check-equal? l0 library))

(test-case "Setting track to playlist"
  ; track-count is "0" for both "foo" and "default"
  (define default (get-playlist library "default"))
  (check-string=? (attr-ref default 'track-count) "0")
  (check-string=? (attr-ref playlist 'track-count) "0")

  ; add track to foo and watch the track-count increment
  (define d1 (set-track default track))
  (define p1 (set-track playlist track))
  (check-string=? (attr-ref d1 'track-count) "1")
  (check-string=? (attr-ref p1 'track-count) "1"))

(test-case "Adding/importing track to playlist"
  ; adding a track is like importing to the library
  (define l1 (add-playlist library playlist))
  (define d1 (get-playlist l1 "default"))
  (define p1 (get-playlist l1 "foo"))

  ; track-count is "0" for both "foo" and "default"
  (check-string=? (attr-ref d1 'track-count) "0")
  (check-string=? (attr-ref p1 'track-count) "0")

  ; adding/importing track to "foo" will also add to "default"
  (define l2 (add-track l1 "foo" track))
  (define d2 (get-playlist l2 "default"))
  (define p2 (get-playlist l2 "foo"))
  ; track-count as incremented by 1
  (check-string=? (attr-ref d2 'track-count) "1")
  (check-string=? (attr-ref p2 'track-count) "1"))

(test-case "Removing track from playlist"
  (define p1 (set-track playlist track))
  (define title (get-track-title track))
  (define path (get-track-path track))
  (check-true (playlist-empty? (del-track/title p1 title)))
  (check-true (playlist-empty? (del-track/path p1 path))))

;(test-case "Purging track from library")
